import com.google.java.contract.*;

@Invariant("d != 0")
public class Fraction {
    int n;
    int d;

    @Requires("d != 0")
    public Fraction(int n, int d) {
        this.n = n;
        this.d = d;
    }

    public void add(Fraction f) {
        n = n * f.d + d * f.n;
        d = d + f.d;
    }

    public int toInteger() {
        return n / d;
    }

    public static void main(String[] args) {
        Fraction f = new Fraction(5, -2);
        Fraction g = new Fraction(3, 2);
        f.add(g);
        // five years later...
        System.out.println(f.toInteger());
    }
}
