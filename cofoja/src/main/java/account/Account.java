package account;

public interface Account {

    public int getBalance();

    public void deposit(int sum);

    public void withdraw(int sum);

}
