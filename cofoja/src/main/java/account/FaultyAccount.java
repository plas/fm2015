package account;

public class FaultyAccount implements Account {

    private int balance = 0;

    public int getBalance() {
        return balance;
    }

    @Override
    public void deposit(int sum) {
        int myShare = sum/100;
        balance += sum - myShare;
    }

    @Override
    public void withdraw(int sum) {
        balance -= sum;
    }


}
