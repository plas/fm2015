package account;

import java.util.LinkedList;
import java.util.List;

public class HistoryAccount implements Account {

    private List<Integer> actions;

    public HistoryAccount() {
        actions = new LinkedList<>();
    }

    public int getBalance() {
        return actions.stream().mapToInt(Integer::intValue).sum();
    }

    @Override
    public void deposit(int sum) {
        actions.add(sum);
    }

    @Override
    public void withdraw(int sum) {
        actions.add(-sum);
    }

}
