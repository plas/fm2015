package account;

public class SavingsAccount implements Account {

    private int balance = 0;

    public int getBalance() {
        return balance;
    }

    @Override
    public void deposit(int sum) {
        balance += sum;
    }

    @Override
    public void withdraw(int sum) {
        balance -= sum;
    }


}
