package mylist;

import com.google.java.contract.Ensures;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class MyArrayList<T> implements MyList<T> {

    int capacity = 16;
    int nextFreeSlot = 0;
    private Object[] elements = new Object[capacity];

    private <T> T old(T x) { return x; }

    public void add(T element) {
        if (nextFreeSlot == capacity) expandArray();
        elements[nextFreeSlot++] = element;
    }

    public boolean contains(Object elem) {
        for (int i = 0; i < nextFreeSlot; i++) {
            if (elements[i].equals(elem)) return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<T> toList() {
        return (List<T>) Arrays.asList(Arrays.copyOf(elements, nextFreeSlot));
    }

    private void expandArray() {
        capacity *= 2;
        elements = Arrays.copyOf(elements, capacity);
        nextFreeSlot--;
    }

    @SuppressWarnings("unchecked")
    public T get(int i) {
        return (T) elements[i];
    }

}
