package mylist;

import com.google.java.contract.Ensures;

import java.util.List;

public interface MyList<T>  {
    public void add(T element);
    public boolean contains(T element);
    public T get(int i);
    public List<T> toList();
}
