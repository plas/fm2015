package stack;

import java.util.ArrayList;

public class ArrayListQueue<T> implements Stack<T> {
    protected ArrayList<T> elements;

    public ArrayListQueue() {
        elements = new ArrayList<>();
    }

    public int size() {
        return elements.size();
    }

    public T peek() {
        return elements.get(0);
    }

    public T pop() {
        return elements.remove(0);
    }

    public void push(T obj) {
        elements.add(obj);
    }
}