package stack;

import java.util.ArrayList;

public class ArrayListStack<T> implements Stack<T> {
    protected ArrayList<T> elements;

    public ArrayListStack() {
        elements = new ArrayList<T>();
    }

    public int size() {
        return elements.size();
    }

    public T peek() {
        return elements.get(elements.size() - 1);
    }

    public T pop() {
        return elements.remove(elements.size() - 1);
    }

    public void push(T obj) {
        elements.add(obj);
    }
}