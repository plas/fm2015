package stack;

import java.util.HashSet;
import java.util.Set;

public class FlawedStack<T> implements Stack<T> {
    protected Set<T> elements;
    private T last;

    public FlawedStack() {
        elements = new HashSet<T>();
    }

    public int size() {
        return elements.size();
    }

    public T peek() {
        return last;
    }

    public T pop() {
        elements.remove(last);
        return last;
    }

    public void push(T obj) {
        last = obj;
        elements.add(obj);
    }
}