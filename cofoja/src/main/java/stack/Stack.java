package stack;

public interface Stack<T> {

    public int size();

    public T peek();

    public T pop();

    public void push(T obj);

}