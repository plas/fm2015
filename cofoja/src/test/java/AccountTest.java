import account.Account;
import account.FaultyAccount;
import account.HistoryAccount;
import account.SavingsAccount;
import com.google.java.contract.ContractAssertionError;
import com.google.java.contract.PostconditionError;
import com.google.java.contract.PreconditionError;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountTest {

    @Test
    public void testSavingAccount() {
        Account account = new SavingsAccount();
        account.deposit(250);
        account.withdraw(50);
        account.deposit(100);
        assertEquals(300, account.getBalance());
    }

    @Test
    public void testHistoryAccount() {
        Account account = new HistoryAccount();
        account.deposit(250);
        account.withdraw(50);
        account.deposit(100);
        assertEquals(300, account.getBalance());
    }

    @Test(expected = PostconditionError.class)
    public void testFaultyAccount() {
        Account account = new FaultyAccount();
        account.deposit(250);
        account.withdraw(50);
        account.deposit(100);
        assertEquals(300, account.getBalance());
    }

    @Test(expected = PreconditionError.class)
    public void testPrecondition() {
        Account account = new SavingsAccount();
        account.withdraw(-50);
        account.deposit(-10);
        assertEquals(40, account.getBalance());
    }

    @Test(expected = ContractAssertionError.class)
    public void testNegativeBalance() {
        Account account = new SavingsAccount();
        account.deposit(50);
        account.withdraw(100);
        account.deposit(50);
        assertEquals(0, account.getBalance());
    }

}