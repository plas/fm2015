import com.google.java.contract.InvariantError;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FractionTest {

    @Test(expected= InvariantError.class)
    public void testAdd() throws Exception {
        Fraction f = new Fraction(3, -2);
        Fraction g = new Fraction(5, 2);
        f.add(g);
        assertEquals(1, f.toInteger());
    }

    @Test
    public void testToInteger() throws Exception {
        assertEquals(4, new Fraction(12, 3).toInteger());

    }
}