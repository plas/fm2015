import com.google.java.contract.PostconditionError;
import mylist.MyArrayList;
import mylist.MyList;
import org.junit.Test;

public class MyArrayListTest {

    @Test(expected= PostconditionError.class)
    public void testAdd() throws Exception {
        MyList<Integer> list = new MyArrayList<>();
        for (int i = 0; i < 20; i++) list.add(i);
        //  System.out.println(mylist.toList());
    }
}