import com.google.java.contract.PostconditionError;
import com.google.java.contract.PreconditionError;
import org.junit.Test;
import stack.ArrayListQueue;
import stack.ArrayListStack;
import stack.FlawedStack;
import stack.Stack;

import static org.junit.Assert.assertEquals;

public class StackTest {

    @Test
    public void testSize() throws Exception {
        Stack<Integer> stack = new ArrayListStack<>();
        stack.push(5);
        stack.push(7);
        stack.push(5);
        stack.push(75);
        assertEquals(4, stack.size());
    }

    @Test(expected= PostconditionError.class)
    public void testFlawedSize() throws Exception {
        Stack<Integer> stack = new FlawedStack<>();
        stack.push(5);
        stack.push(7);
        stack.push(5);
        stack.push(75);
        assertEquals(4, stack.size());
    }

    @Test(expected= PreconditionError.class)
    public void testPeek() throws Exception {
        Stack<Integer> stack = new ArrayListStack<>();
        stack.peek();
    }

    @Test
    public void testPopOnce() throws Exception {
        Stack<Integer> stack = new FlawedStack<>();
        stack.push(5);
        stack.push(7);
        stack.push(75);
        stack.pop();
        assertEquals(2, stack.size());
    }

    @Test(expected= PostconditionError.class)
    public void testPopTwice() throws Exception {
        Stack<Integer> stack = new FlawedStack<>();
        stack.push(5);
        stack.push(7);
        stack.push(75);
        stack.pop();
        stack.pop();
        assertEquals(1, stack.size());
    }

    @Test(expected= PostconditionError.class)
    public void testQueue() throws Exception {
        Stack<Integer> stack = new ArrayListQueue<>();
        stack.push(5);
        stack.push(7);
        stack.push(75);
        stack.pop();
        assertEquals(2, stack.size());
    }

}