import java.util.Set;
import java.util.function.Predicate;

public abstract class AbstractMultiset<T extends Comparable<T>> implements Multiset<T> {

    abstract public T getMin();

    abstract public void removeDups();

    protected boolean forall(Predicate<T> p) {
        return copyElems().stream().allMatch(p);
    }

    abstract protected Set<T> copyElems();

}
