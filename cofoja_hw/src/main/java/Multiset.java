public interface Multiset<T extends Comparable<T>> {

    public boolean contains(T elem);
    public int count(T elem);

    public void add(T elem);

    public void remove(T elem);

    public void removeDups();

    public T getMin();

}
