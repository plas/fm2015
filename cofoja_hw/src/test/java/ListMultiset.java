import java.util.*;
import java.util.stream.Collectors;

public class ListMultiset<T extends Comparable<T>> extends AbstractMultiset<T> {

    private List<T> elems;

    public ListMultiset() {
        elems = new ArrayList<>();
    }

    @Override
    public boolean contains(T elem) {
        return elems.contains(elem);
    }

    @Override
    public int count(T elem) {
        int i = 0;
        for (T e : elems) {
            if (e.equals(elem)) i++;
        }
        return i;
    }

    @Override
    public void add(T elem) {
        elems.add(elem);
    }

    @Override
    public void remove(T elem) {
        throw new NoSuchElementException();
    }

    @Override
    public void removeDups() {
        elems = elems.stream().filter(e -> count(e) == 1).collect(Collectors.toList());
    }

    @Override
    public T getMin() {
        return elems.iterator().next();
    }

    @Override
    protected Set<T> copyElems() {
        return new HashSet<>(elems);
    }
}
