import java.util.*;

public class MapMultiset<T extends Comparable<T>> extends AbstractMultiset<T> {
    private Map<T, Integer> index;

    public MapMultiset() {
        index = new TreeMap<>();
    }

    @Override
    public boolean contains(T elem) {
        return index.containsKey(elem);
    }

    @Override
    public int count(T elem) {
        Integer count = index.get(elem);
        return count == null ? 0 : count;
    }

    @Override
    public void add(T elem) {
        int currentCount = count(elem);
        index.put(elem, currentCount + 1);
    }

    @Override
    public void remove(T elem) {
        if (!contains(elem)) throw new NoSuchElementException();
        int currentCount = count(elem);
        index.put(elem, currentCount - 1);
    }

    @Override
    public void removeDups() {
        for (T elem : index.keySet()) index.put(elem, 1);
    }

    private List<T> toList() {
        List<T> list = new ArrayList<T>();
        for (Map.Entry<T, Integer> entry : index.entrySet()) {
            for (int i = 0; i < entry.getValue(); i++) {
                list.add(entry.getKey());
            }
        }
        return list;
    }

    @Override
    public T getMin() {
        return index.keySet().iterator().next();
    }

    @Override
    protected Set<T> copyElems() {
        return new HashSet<>(index.keySet());
    }
}
