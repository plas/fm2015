import com.google.java.contract.InvariantError;
import com.google.java.contract.PostconditionError;
import com.google.java.contract.PreconditionError;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Random;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MultisetTest {


    @Test(expected = PreconditionError.class)
    public void test01_AddNull()  {
        Multiset<Integer> multiset = new MapMultiset<>();
        multiset.add(5);
        multiset.add(null);
    }

    @Test(expected = PostconditionError.class)
    public void test02_AddSet()  {
        Multiset<Integer> multiset = new SetMultiset<>();
        multiset.add(5);
        multiset.add(7);
        multiset.add(5);
    }

    @Test(expected = PostconditionError.class)
    public void test03_RemoveSetEmpty()  {
        Multiset<Integer> multiset = new SetMultiset<>();
        multiset.remove(5);
    }

    @Test
    public void test04_RemoveMap()  {
        Multiset<Integer> multiset = new MapMultiset<>();
        multiset.add(5);
        multiset.add(7);
        multiset.add(5);
        multiset.remove(5);
    }

    @Test(expected = PostconditionError.class)
    public void test05_RemoveList()  {
        Multiset<Integer> multiset = new ListMultiset<>();
        multiset.add(5);
        multiset.remove(5);
    }

    @Test
    public void test06_GetMinMap()  {
        Multiset<Integer> multiset = new MapMultiset<>();
        testGetMin(multiset);
    }

    @Test(expected = PostconditionError.class)
    public void test07_GetMinList()  {
        Multiset<Integer> multiset = new ListMultiset<>();
        testGetMin(multiset);
    }

    private void testGetMin(Multiset<Integer> multiset) {
        Random random = new Random();
        int first = random.nextInt();
        multiset.add(first);
        int min = first;
        for (int i = 0; i < 100; i++) {
            int value = random.nextInt(100);
            multiset.add(value);
            min = Math.min(min, value);
        }
        // Avoiding the case where the first element happens to be the minimum.
        if (first == min) multiset.add(--min);
        int claimedMin = multiset.getMin();
        assertEquals(min, claimedMin);
    }

    @Test
    public void test08_RemoveDups()  {
        Multiset<Integer> multiset = new MapMultiset<>();
        for (int i = 0; i < 50; i++) multiset.add(i);
        for (int i = 49; 0 <= i; i--) multiset.add(i);
        for (int i = 0; i < 25; i++) multiset.add(i);
        multiset.removeDups();
    }

    @Test(expected = PostconditionError.class)
    public void test09_RemoveDupsList()  {
        Multiset<Integer> multiset = new ListMultiset<>();
        for (int i = 0; i < 50; i++) multiset.add(i);
        for (int i = 49; 0 <= i; i--) multiset.add(i);
        for (int i = 0; i < 25; i++) multiset.add(i);
        multiset.removeDups();
    }

    @Test(expected = InvariantError.class)
    public void test10_RemoveMap()  {
        Multiset<Integer> multiset = new MapMultiset<>();
        multiset.add(5);
        multiset.add(7);
        multiset.add(5);
        multiset.remove(7);
        assertTrue(multiset.contains(5));
        // The MapMultiset still believes it contains(7).
        assertFalse(multiset.contains(7));
    }

}