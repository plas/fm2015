import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetMultiset<T extends Comparable<T>> extends AbstractMultiset<T> {

    private Set<T> elems;

    public SetMultiset() {
        elems = new TreeSet<>();
    }

    @Override
    public boolean contains(T elem) {
        return elems.contains(elem);
    }

    @Override
    public int count(T elem) {
        return elems.contains(elem) ? 1 : 0;
    }

    @Override
    public void add(T elem) {
        elems.add(elem);
    }

    @Override
    public void remove(T elem) {
        elems.remove(elem);
    }

    @Override
    public void removeDups() {
        return;
    }

    @Override
    public T getMin() {
        return elems.iterator().next();
    }

    @Override
    protected Set<T> copyElems() {
        return new HashSet<>(elems);
    }
}
