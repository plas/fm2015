#!/usr/bin/env bash

echo 'yes' | sudo add-apt-repository ppa:avsm/ppa
sudo apt-get update -q
sudo apt-get install -y ocaml ocaml-native-compilers camlp4-extra opam
sudo apt-get install -y m4 libgmp-dev libgtksourceview2.0-dev
#sudo apt-get install -y -m xfce4 lightdm virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-X11
#sudo sh -c "echo 'allowed_users=anybody' > /etc/X11/Xwrapper.config"
sudo sh -c "echo 'LC_ALL=en_US.UTF-8\nLANG=en_US.UTF-8' >> /etc/environment"

export OPAMYES=1
opam init -a
eval `opam config env`
opam install alt-ergo why3
opam install coq.8.4.5 coqide.8.4.5
wget -nv "https://www.lri.fr/~marche/MPRI-2-36-1/z3-4.3.2"
chmod +x z3-4.3.2 && sudo mv z3-4.3.2 /usr/local/bin/z3
wget -nv http://cvc4.cs.nyu.edu/builds/x86_64-linux-opt/cvc4-1.4-x86_64-linux-opt
chmod +x cvc4-1.4-x86_64-linux-opt && sudo mv cvc4-1.4-x86_64-linux-opt /usr/local/bin/cvc4
why3 config --detect
